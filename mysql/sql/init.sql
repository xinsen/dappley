/*
Navicat MySQL Data Transfer

Source Server         : 192.168.1.65
Source Server Version : 50718
Source Host           : 192.168.1.65:3306
Source Database       : dappley-web

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2018-11-01 16:01:33
*/
create database dappley;
use dappley;

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_block
-- ----------------------------
DROP TABLE IF EXISTS `t_block`;
CREATE TABLE `t_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `index` bigint(20) DEFAULT NULL COMMENT '区块高度',
  `hash` varchar(64) DEFAULT NULL,
  `pre_hash` varchar(64) DEFAULT NULL,
  `nonce` bigint(20) DEFAULT NULL,
  `timestamp` bigint(20) DEFAULT NULL COMMENT '区块挖出时间',
  `transactions` varchar(64) DEFAULT NULL COMMENT '最终的交易hash',
  `miner` varchar(64) DEFAULT NULL COMMENT '挖矿者',
  `size` int(11) DEFAULT NULL COMMENT '区块字节大小',
  `crt_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9196 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for t_transaction
-- ----------------------------
DROP TABLE IF EXISTS `t_transaction`;
CREATE TABLE `t_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `blc_id` bigint(20) DEFAULT NULL COMMENT '关联区块id',
  `hash` varchar(64) DEFAULT NULL,
  `crt_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9215 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for t_tx_input
-- ----------------------------
DROP TABLE IF EXISTS `t_tx_input`;
CREATE TABLE `t_tx_input` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) DEFAULT NULL COMMENT '所属交易id',
  `tx_hash` varchar(64) DEFAULT NULL COMMENT '交易hash',
  `tx_out` int(11) DEFAULT NULL COMMENT '未花费索引',
  `pub_key` varchar(255) DEFAULT NULL COMMENT '身份id',
  `crt_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9213 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for t_tx_out
-- ----------------------------
DROP TABLE IF EXISTS `t_tx_out`;
CREATE TABLE `t_tx_out` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) DEFAULT NULL,
  `index` int(11) DEFAULT NULL,
  `amount` bigint(20) DEFAULT NULL,
  `pub_key_hash` varchar(255) DEFAULT NULL,
  `crt_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9223 DEFAULT CHARSET=utf8mb4;
