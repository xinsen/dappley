#!/usr/bin/env bash
echo "entry exec..."
./wait-for-it.sh mysql:3306 --timeout=45
java -Dspring.profiles.active=env -Dspring.datasource.druid.url=${SPRING_DRUID_URL} -Dspring.datasource.druid.password=${MYSQL_ROOT_PASSWORD} -Ddappley.server.ip=${DAPPLEY_NOTE_IP} -Ddappley.server.port=${DAPPLEY_NOTE_PORT} -jar /app.jar